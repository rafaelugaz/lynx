import {Component, Input, OnInit} from '@angular/core';
import {FormElement} from '../form-element';

@Component({
  selector: 'app-form-step',
  templateUrl: './form-step.component.html',
  styleUrls: ['./form-step.component.css']
})
export class FormStepComponent implements OnInit {
  @Input() public title: string;
  @Input() public model: any;
  @Input() public fields: FormElement[];

  constructor(
    ) { }

  ngOnInit(): void {
  }
}
