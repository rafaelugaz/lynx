import {Component} from '@angular/core';
import {FieldType, FormElement} from './form-element';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'lynx';

	currentStep: number = 0; // The current step displayed
	inProgress: boolean = true;

	model: any = {}

	previous() {
		this.currentStep--;
	}

	next() {
		if (this.isLastStep()) {
			this.inProgress = false;
		} else {
			this.currentStep++;
		}
	}

	isFirstStep() {
		return this.currentStep == 0;
	}

	isLastStep() {
		console.log(this.currentStep);
		console.log(this.steps.length);
		
		return this.currentStep == this.steps.length - 1;
	}

	restartForm() {
		this.currentStep = 0;
		this.inProgress = true;
		this.model = {};
	}

	steps: {title:string, fields: FormElement[]}[] = [
		{
			title: "Step 1",
			fields: [
				{
					label: "Name",
					key: "name",
					type: FieldType.Text
				},
				{
					label: "Age",
					key: "age",
					type: FieldType.Number
				},
				{
					label: "Already a client",
					key: "is_client",
					type: FieldType.Boolean
				}
			]
		},
		{
			title: "Step 2",
			fields: [
				{
					label: "Client number ELENA",
					key: "client_id",
					type: FieldType.Boolean,
					dependsOn: "age"
				},
				{
					label: "Flag1",
					key: "flag",
					type: FieldType.Boolean
				},
				{
					label: "Flag2",
					key: "flag2",
					type: FieldType.Boolean
				},
				{
					label: "Flag3",
					key: "flag3",
					type: FieldType.Boolean
				},
				{
					label: "Text4",
					key: "text4",
					type: FieldType.Text
				}
			]
		},
		{
			title: "Step 3",
			fields: [
				{
					label: "Have bank account?",
					key: "has_account",
					type: FieldType.Boolean
				},
				{
					label: "IBAN",
					key: "iban",
					type: FieldType.Text,
					dependsOn: "has_account"
				},
				{
					label: "A number",
					key: "age",
					type: FieldType.Number
				},
				{
					label: "Flag44",
					key: "flag44",
					type: FieldType.Boolean
				},
				{
					label: "Flag33",
					key: "flag33",
					type: FieldType.Boolean
				},
				{
					label: "num3",
					key: "num3",
					type: FieldType.Number
				},
				{
					label: "num4",
					key: "num4",
					type: FieldType.Number
				}
			]
		},
		{
			title: "Step 4",
			fields: [
				{
					label: "Have bank account?",
					key: "has_account",
					type: FieldType.Boolean
				},
				{
					label: "IBAN",
					key: "iban",
					type: FieldType.Text,
					dependsOn: "has_account"
				},
				{
					label: "A number",
					key: "age",
					type: FieldType.Number
				},
				{
					label: "Flag44",
					key: "flag44",
					type: FieldType.Boolean
				},
				{
					label: "Flag33",
					key: "flag33",
					type: FieldType.Boolean
				},
				{
					label: "num3",
					key: "num3",
					type: FieldType.Number
				},
				{
					label: "num4",
					key: "num4",
					type: FieldType.Number
				},
				{
					label: "Have bank account?",
					key: "has_account",
					type: FieldType.Boolean
				},
				{
					label: "IBAN",
					key: "iban",
					type: FieldType.Text,
					dependsOn: "has_account"
				},
				{
					label: "A number",
					key: "age",
					type: FieldType.Number
				},
				{
					label: "Flag44",
					key: "flag44",
					type: FieldType.Boolean
				},
				{
					label: "Flag33",
					key: "flag33",
					type: FieldType.Boolean
				},
				{
					label: "num3",
					key: "num3",
					type: FieldType.Number
				},
				{
					label: "num4",
					key: "num4",
					type: FieldType.Number
				}
			]
		}
	];
}