import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  @Input() public model: any;
  @Output("restart") onRestart = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void { }

  download() {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/json;charset=utf-8,' +
      encodeURIComponent(JSON.stringify(this.model)));
    element.setAttribute('download', "summary.json");
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);
  }

  restart() {
    this.onRestart.emit();
  }

}
