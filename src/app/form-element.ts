export enum FieldType {
	Text = "text",
	Boolean = "checkbox",
	Number = "number"
}

export interface FormElement {
	label: string,
	key: string,
	type: FieldType,
	dependsOn?: string
}
